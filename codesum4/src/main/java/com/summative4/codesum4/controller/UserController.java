package com.summative4.codesum4.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.summative4.codesum4.model.Department;
import com.summative4.codesum4.model.User;
import com.summative4.codesum4.repository.DepartmentRepository;
import com.summative4.codesum4.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepo;

    @Autowired
    private DepartmentRepository departRepo;

    @GetMapping("/users")
    public String showCreateForm(Model model) {
        List<Department> listDepartments = departRepo.findAll();
        List<User> listUsers = userRepo.findAll();

        model.addAttribute("listDepartments", listDepartments);
        model.addAttribute("listUsers", listUsers);
        return "users";
    }

    @PostMapping(value = "/user/save", consumes = "application/json")
    public String addUser(@RequestBody User user) {
        userRepo.save(user);
        return "redirect:/users";
   }

    @PostMapping(value = "/user/saveAll", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String addUserAll(@RequestBody ArrayList<User> user) {
        userRepo.saveAll(user);
        return "redirect:/users";
    }


    @PutMapping("/user/update/{id}")
    public ResponseEntity<Object> updateUser(
            @RequestBody User user,
            @PathVariable int id) {

        Optional<User> opRepo
                = Optional.ofNullable(
                        userRepo.findById(id));

        if (!opRepo.isPresent()) {
            return ResponseEntity
                    .notFound()
                    .build();
        }

        user.setId(id);

        userRepo.save(user);

        return ResponseEntity
                .noContent()
                .build();
    }

    @GetMapping("/user/delete/{id}")
	public String deleteUser(
			@PathVariable("id") int id) {
		
		userRepo.deleteById(id);
		
		return "redirect:/users";
	}
}
