package com.summative4.codesum4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Codesum4Application {

	public static void main(String[] args) {
		SpringApplication.run(Codesum4Application.class, args);
	}

}
