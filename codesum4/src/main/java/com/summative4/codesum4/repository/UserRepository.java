package com.summative4.codesum4.repository;

import java.util.List;

import com.summative4.codesum4.model.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer>{
    User findById(int id);
    List<User> findAll();
    void deleteById(int id);
}
