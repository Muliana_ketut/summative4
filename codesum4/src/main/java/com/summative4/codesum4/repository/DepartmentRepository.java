package com.summative4.codesum4.repository;

import com.summative4.codesum4.model.Department;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Integer>{
    
}
